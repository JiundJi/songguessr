# songguessr
guess the song but as an app (requires spotify premium (for now))

## Usage
```cargo tauri dev```
for the dev build

## Roadmap
- finish this
- add other platforms

## Authors and acknowledgment
thanks to [rspotify](https://github.com/ramsayleung/rspotify) and [tauri](https://tauri.app)

## License
GNU GPL v3

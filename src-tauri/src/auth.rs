use futures_util::{pin_mut, StreamExt};
use rspotify::{clients::{BaseClient, OAuthClient}, *};

#[tauri::command]
pub async fn auth_start() -> String {

    let spotify = auth().await;
    
    // execute requests
    let user = spotify.me().await.expect("failed to fetch user");
    println!("{user:?}");
    
    let username = user.display_name.unwrap_or("error reading username".to_string());
    println!("{username:?}");

    return username

}

async fn auth() -> AuthCodeSpotify {

    let scopes = scopes!(
        "user-read-private",
        "user-library-read",
        "user-read-playback-state",
        "user-read-playback-position",
        "user-modify-playback-state",
        "playlist-read-collaborative",
        "playlist-read-private"
    );

    let creds = 
        if Credentials::from_env().is_none() {
            Credentials::new("1a2cf9d6b8944bd2b1a7049993edd419", "2be77b60ad6445f694941a9ce23869db")
        } else {
            Credentials::from_env().unwrap()
        };

    let oauth = OAuth::from_env(scopes).unwrap();
    let conf = Config {
            token_cached: true,
            token_refreshing: true,

            ..Default::default()
        };

    let spotify = AuthCodeSpotify::with_config(creds.clone(), oauth, conf);
    let url = spotify.get_authorize_url(false).unwrap();
    
    spotify.prompt_for_token(&url).await.expect("auth went wrong");

    return spotify
}

#[tauri::command]
pub async fn get_playlist_names() -> Vec<String> {

    let spotify = auth().await;
    
    let user = spotify.me().await.expect("failed to fetch user");
    let user_id = user.id;
    
    let playlists = spotify.user_playlists(user_id);

    let mut result: Vec<String> = vec![];
    pin_mut!(playlists);
    while let Ok(i) = playlists.next().await.unwrap() {
        result.push(i.name);
    }

    return result
}

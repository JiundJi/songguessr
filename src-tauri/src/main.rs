// Prevents additional console window on Windows in release, DO NOT REMOVE!!
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

use tauri::generate_context;

mod ui;
mod auth;
use auth::*;

fn main() {
    tauri::Builder::default()
        .invoke_handler(tauri::generate_handler![auth_start])
        .invoke_handler(tauri::generate_handler![get_playlist_names])
        .run(generate_context!())
        .expect("error while running tauri application");
}

